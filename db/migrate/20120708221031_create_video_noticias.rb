class CreateVideoNoticias < ActiveRecord::Migration
  def change
    create_table :video_noticias do |t|
      t.references :noticia
      
      t.string :url
      t.has_attached_file :capa

      t.timestamps
    end
    add_index :video_noticias, :noticia_id
  end
end
