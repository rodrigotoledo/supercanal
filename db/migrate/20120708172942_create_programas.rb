class CreateProgramas < ActiveRecord::Migration
  def change
    create_table :programas do |t|
      t.references :usuario
      t.string :nome
      t.text :descricao
      t.string :cor, size: 10
      t.integer :posicao, size: 2

      t.timestamps
    end
    add_index :programas, :usuario_id
  end
end
