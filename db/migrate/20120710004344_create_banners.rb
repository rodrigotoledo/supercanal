class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.string :titulo
      t.has_attached_file :arquivo
      t.boolean :ativo

      t.timestamps
    end
  end
end
