class CreateCategoriaNoticias < ActiveRecord::Migration
  def change
    create_table :categoria_noticias do |t|
      t.references :noticia
      t.references :categoria

      t.timestamps
    end
    add_index :categoria_noticias, :noticia_id
    add_index :categoria_noticias, :categoria_id
  end
end
