class ImagemNoticia < ActiveRecord::Base
  belongs_to :noticia
  attr_accessible :arquivo

  has_attached_file :arquivo, styles: { mini: "100x100#", normal: '338x240#' },
                    default_style: :normal
end
