class Programa < ActiveRecord::Base
  belongs_to :usuario
  attr_accessible :cor, :descricao, :nome, :posicao
  has_many :noticias

  validates_presence_of :nome, :cor, :descricao, :posicao
  
  def to_param
    nome.parameterize
  end
end
