class VideoPrograma < ActiveRecord::Base
  belongs_to :programa
  attr_accessible :capa, :titulo, :url, :pagina_inicial
  
  has_attached_file :capa, styles: { mini: "100x100#", normal: '338' },
                    default_style: :normal
  validates_presence_of :titulo, :url
end
