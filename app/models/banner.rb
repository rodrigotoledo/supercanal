class Banner < ActiveRecord::Base
  attr_accessible :arquivo, :ativo, :titulo
  has_attached_file :arquivo, styles: { mini: "100x100#", normal: '338x240#' },
                    default_style: :normal
  validates_presence_of :titulo
  validates :arquivo, :attachment_presence => true
end
