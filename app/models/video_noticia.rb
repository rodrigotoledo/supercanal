class VideoNoticia < ActiveRecord::Base
  belongs_to :noticia
  attr_accessible :url, :capa, :titulo, :pagina_inicial
  
  has_attached_file :capa, styles: { mini: "100x100#", normal: '338' },
                    default_style: :normal
                    
  before_save :default_values
  
  def default_values
    if self.titulo.blank?
      self.titulo = self.noticia.titulo
    end
    true
  end
end
